import React, {Component} from 'react';

import './Progress.scss';

import CountUp from 'react-countup';

class ProgressRing extends Component {
  constructor(props) {
    super(props);

    const { radius, stroke } = this.props;

    this.normalizedRadius = radius - stroke * 2;
    this.circumference = this.normalizedRadius * 2 * Math.PI;
  }

  render() {
  	const { radius, stroke, progress } = this.props;
  	const strokeDashoffset = this.circumference - progress / 100 * this.circumference;

	  return (
	  	<div className="ProgressContainer">

		  	<div className="Progress">
		  		<svg
			      height={radius * 2}
			      width={radius * 2}
			      className="ProgressRingBG"
			      >

			      <circle
			        stroke="#898989"
			        fill="transparent"
			        strokeWidth={ stroke }
			        strokeDasharray={ this.circumference + ' ' + this.circumference }
			        strokeWidth={ stroke }
			        r={ this.normalizedRadius }
			        cx={ radius }
			        cy={ radius }
			        />
			    </svg>
			    <svg
			      height={radius * 2}
			      width={radius * 2}
			      className="ProgressRing"
			      >
			      <defs>
				      <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
					      <stop offset="0%" stopColor="#fd9531" />
					      <stop offset="100%" stopColor="#fa782c" />
					  </linearGradient>
				  </defs>
			      <circle
			        stroke="url(#gradient)"
			        fill="transparent"
			        strokeWidth={ stroke }
			        strokeDasharray={ this.circumference + ' ' + this.circumference }
			        style={ { strokeDashoffset } }
			        strokeWidth={ stroke }
			        r={ this.normalizedRadius }
			        cx={ radius }
			        cy={ radius }
			        />
			    </svg>
			    <span className="ProgressTotal" style={{
			    	bottom: this.props.radius,
			    	left:this.props.radius,
			    	transform: 'translate(-50%,50%)'
			    }}>
			    	{progress}<CountUp end={progress} delay={1} duration={5}/>%
			    	
			    </span>
		    </div>
	    	<span className="progressTitle" style={{display:'block', width: this.props.radius*2, textAlign:'center'}}>{this.props.title}</span>
	    </div>
	  );
	}
}

export default ProgressRing;