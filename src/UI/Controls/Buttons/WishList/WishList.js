import React from 'react';

const WishListButton = (props) => {
	let Added = ''
	if (props.Added === true){
		Added = 'added';
	}
	return(
		<button className={Added} onClick={props.clicked}><i className={props.fontAwesome}></i></button>
	)
};

export default WishListButton;