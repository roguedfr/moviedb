import React from 'react';

const Star = (props) => {
	const displayStar = props.rated === 'true';

	if (displayStar){
		return <i className="fas fa-star"></i>
	} else {
		return <i className="far fa-star"></i>
	}
};

export default Star;