import React from 'react';

import Star from './Star/Star';

const Rating = (props) => {
	const total = 5;
	return [...Array(total)].map((e, i) => {
			if (i >= props.rating){
				return <button key={i} onClick={() => props.clicked(i)}><Star/></button>
			} else {
				return <button key={i} onClick={() => props.clicked(i)}><Star rated="true"/></button>
			}
		}
	)
}

export default Rating;