import React, {Component} from 'react';

import './Options.scss';

class Options extends Component {
	constructor() {
		super();
		this.state = {
			movieId : '',
		};
		this.inputChange = this.inputChange.bind(this);
	}

	setMovieHandler = () => {
		this.props.movieIdValue(this.state.movieId);
	}

	inputChange({ target }) {
	    this.setState({
	    	movieId: target.value
	    });
	}

	render(){
		let attachedClasses = ['OptionsContainer', 'Close']
		if (this.props.Show){
			attachedClasses = ['OptionsContainer', 'Open']
		}
		return(
			<div className={attachedClasses.join(' ')}>
				<button className="close-button" aria-label="Close alert" type="button" onClick={this.props.click}>
				    <span aria-hidden="true">&times;</span>
				</button>
				<div className="grid-container">
				    <div className="grid-x grid-padding-x">
				    	<div className="medium-12 cell">
				    		<h3 className="text-center">Settings</h3>
				    		<hr/>
				    	</div>
				    	<div class="medium-12 cell">
							<label><h5><i className="fas fa-film"></i> Movie ID</h5>
							<div class="input-group">
  								<input className="input-group-field" type="number" placeholder="Enter Movie ID" value={this.state.movieId} onChange={this.inputChange}/>
  								<div className="input-group-button">
							    	<button type="submit" className="button" onClick={this.setMovieHandler}>View</button>
								</div>
							</div>
							</label>
						</div>
						<div class="medium-12 cell">
							<p>Movie ID Examples<br/>
								335983 - Venom<br/>
								424694 - Bohemian Rhapsody<br/>
								428078 - Mortal Engines<br/>
								446807 - The Girl in the Spiders Web<br/>
								300668 - Annihliation<br/>
								299536 - Avengers Infinity War<br/>
								399579 - Alita: Battle Angel<br/>
								320288 - Dark Phoenix</p>
						</div>
					</div>
				</div>
				<div className="SettingsButton">
					<button onClick={this.props.click}>
						<i class="fas fa-cog"></i>
					</button>
				</div>
			</div>
		)
	}
}

export default Options;