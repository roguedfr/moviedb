import React, {Component} from 'react';

import './Modal.scss';
import Aux from '../../hoc/Auxilary';
import Backdrop from '../Backdrop/Backdrop';

class modal extends Component {

	shouldComponentUpdate(nextProps, nextState){
		return nextProps.show !== this.props.show;
	}

	render (){
		return (
			<Aux>
				<Backdrop show={this.props.show} clicked={this.props.modalClosed}/>
				<div
					className="Modal"
					style={{
						visibility: this.props.show ? 'visible' : 'hidden',
						opacity: this.props.show ? '1' : '0'
					}}>
					{this.props.children}
				</div>
			</Aux>
		)
	}
};

export default modal;