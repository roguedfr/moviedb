import * as actionTypes from './actions';

export const showTrailer = () => {
	return {
		type: actionTypes.SHOWTRAILER
	};
};

export const hideTrailer = () => {
	return {
		type: actionTypes.HIDETRAILER
	};
};