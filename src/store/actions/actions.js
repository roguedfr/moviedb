export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';

export const HIDETRAILER = 'HIDETRAILER';
export const SHOWTRAILER = 'SHOWTRAILER';