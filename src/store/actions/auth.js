import Axios from 'axios';

import * as actionTypes from './actions';

export const authStart = () => {
	return {
		type: actionTypes.AUTH_START
	};
};

export const authSuccess = (authData) => {
	return{
		type: actionTypes.AUTH_SUCCESS,
		authData: authData
	};
};

export const authFail = (error) => {
	return{
		type: actionTypes.AUTH_FAIL,
		error: error
	};
};

export const auth = (username, password) => {
	return dispatch => {
		dispatch(authStart());
		const authData = {
			username: username,
			password: password,
			request_token: ''
		}
		Axios.post('authentication/token/validate_with_login?api_key=a5ccc05c247dfd2c49d147b2a043eafa', authData)
		.then(response => {
			console.log(response);
			dispatch(authFail(response.data));
		})
		.catch( err => {
			console.log(err)
			dispatch(authFail(err.data))
		})
	}
}