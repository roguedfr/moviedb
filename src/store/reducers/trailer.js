import * as  actionTypes from '../actions/actions';
import {updateObject} from '../utility';

const initialState ={
	showTrailer: false,
}

const reducer = (state = initialState, action) => {
	switch (action.type){
		case actionTypes.SHOWTRAILER:
			return updateObject(state, {showTrailer: true});
		case actionTypes.HIDETRAILER:
			return updateObject(state, {showTrailer: false});
	}
	return state;
}

export default reducer;