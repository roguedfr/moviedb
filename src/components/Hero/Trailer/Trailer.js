import React from 'react';

import './Trailer.scss';

import Aux from '../../../hoc/Auxilary';

const Trailer = (props) => (
	<Aux>
		<span className="Trailer">
			<button onClick={props.openTrailer}>
				<i className="fas fa-play"></i><span>Watch Trailer</span>
			</button>
		</span>
	</Aux>

);

export default Trailer