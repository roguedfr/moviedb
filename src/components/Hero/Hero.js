import React, {Component} from 'react';
import Axios from 'axios';
import { connect } from 'react-redux';

import './Hero.scss';

import Trailer from './Trailer/Trailer';
import Progress from '../../UI/Progress/Progress';
import WishList from '../../UI/Controls/Buttons/WishList/WishList';
import Rating from '../../UI/Controls/Buttons/Rating/Rating';
import Modal from '../../UI/Modal/Modal';
import YouTube from '../../UI/Video/YouTube/Youtube';

import * as actionCreators from '../../store/actions/index';


class Hero extends Component {

	state = {
		id: this.props.MovieId,
		genres: [],
		video:[]
	}

	componentWillMount(){
		Promise.all([
			Axios.get('/movie/' + this.state.id + '?api_key=a5ccc05c247dfd2c49d147b2a043eafa'),
			Axios.get('movie/' + this.state.id + '/videos?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
		])
			.then(([movieResponse, videoResponse]) => {
				this.setState({genres: movieResponse.data.genres, video: videoResponse.data.results});
			});
	}

	componentWillReceiveProps(nextProps){
		if (nextProps.MovieId !== this.state.id) {
			this.setState({id: nextProps.MovieId})
		}
		
	}

	componentDidUpdate(prevProps, prevState){
		if (prevState.id !== this.state.id) {
		    Promise.all([
				Axios.get('/movie/' + this.state.id + '?api_key=a5ccc05c247dfd2c49d147b2a043eafa'),
			Axios.get('movie/' + this.state.id + '/videos?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
			])
				.then(([movieResponse, videoResponse]) => {
				this.setState({genres: movieResponse.data.genres, video: videoResponse.data.results});
			});
		  }
	}

	showTrailerHandler = (event) => {
		this.setState({showTrailer: true, playTrailer: true});
	}

	closeTrailerHandler = () => {
		this.setState({showTrailer: false})
	}
		

	render(){

		const UserRating = this.props.UserRating*10;

		return(
			<React.Fragment>
			{this.state.video.length ?
				<Modal show={this.props.showTrailer} modalClosed={this.props.onTrailerDeactivate}>
				{this.state.video.slice(0, 1).map(videoKey => {
					return <YouTube 
						Width='65vw'
						Id={videoKey.key}
						AutoPlay="true"
						show={this.props.showTrailer}/>
				})}
				</Modal>
			: null}
			<section className="grid-x grid-padding-x heroImage align-stretch" style={{backgroundImage:'url(https://image.tmdb.org/t/p/original/'+this.props.BackgroundImage+')'}}>
				<div className="grid-container small-12 cell grid-x">
					<div className="grid-x grid-padding-x small-12 align-bottom">
						<div className="small-8 cell">
							<div className="genres">
								{this.state.genres.map(genKey => {
									return <span key={genKey.id}>{genKey.name}</span>
								})}
							</div>
							<h1>{this.props.Name}</h1><span className="Year">({this.props.Year})</span>
							<div className="trailer">
								<Trailer openTrailer={this.props.onTrailerActivate}/>
							</div>
							<div className="grid-x grid-padding-x grid-12 UserOptions">
								<div className="cell small-1">
									<WishList Added={this.props.ListAdded} clicked={this.props.ListClicked} fontAwesome='fas fa-list'/>
								</div>
								<div className="cell small-1">
									<WishList Added={this.props.BookmarkAdded} clicked={this.props.BookmarkClicked} fontAwesome='fas fa-heart'/>
								</div>
								<div className="cell small-1">
									<WishList Added={this.props.WatchListAdded} clicked={this.props.WatchListClicked} fontAwesome='fas fa-bookmark'/>
								</div>
								<div className="cell small-2 star-rating">
									<Rating rating={this.props.Rating} clicked={this.props.RatingChoice}/>
								</div>
							</div>
						</div>
						<div className="small-4 cell">
							<Progress radius={70} stroke={5} progress={UserRating} displayNumber={true} title="User Score"/>
						</div>
					</div>
				</div>
			</section>
			</React.Fragment>
		)
	}
	
}

const mapStateToProps = state => {
	return {
		showTrailer: state.showTrailer.showTrailer
	};
}

const mapDispatchToProps = dispatch => {
	return {
		onTrailerActivate: () => dispatch(actionCreators.showTrailer()),
		onTrailerDeactivate: () => dispatch(actionCreators.hideTrailer())
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Hero);