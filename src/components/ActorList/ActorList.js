import React, {Component} from 'react';
import Axios from 'axios';

import './ActorList.scss';

import Actor from './Actor/Actor';

class ActorList extends Component{

	state = {
		id : this.props.MovieId,
		actors: []
	}


	componentWillMount(){
		Promise.all([
			Axios.get('movie/' + this.state.id + '/credits?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
		])
			.then(([actorsResponse]) => {
				this.setState({actors: actorsResponse.data.cast});
			});
	}

	componentWillReceiveProps(nextProps){
		if (nextProps.MovieId !== this.state.id) {
			this.setState({id: nextProps.MovieId})
		}
		
	}

	componentDidUpdate(prevProps, prevState){
		if (prevState.id !== this.state.id) {
		    Promise.all([
			Axios.get('movie/' + this.state.id + '/credits?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
		])
			.then(([actorsResponse]) => {
				this.setState({actors: actorsResponse.data.cast});
			});
		}
	}

	render(){
		return(
			<div className="ActorList section">
				<div className="grid-container">
					<div className="small-12 cell grid-x align-center">
						<h3 className="Title">Top Billed Cast</h3>
					</div>
					<div className="small-12 cell grid-x small-up-2 medium-up-3 large-up-6">
						{this.state.actors.slice(0, 6).map((actorKey, i) => {
							return <Actor 
										key={actorKey.id}
										id={actorKey.id}
										name={actorKey.name}
										image={actorKey.profile_path}
										character={actorKey.character}/>
						})}
					</div>
				</div>
			</div>
		)
	}
}

export default ActorList;