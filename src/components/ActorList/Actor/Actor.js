import React from 'react';
import { Link } from 'react-router-dom';

import './Actor.scss';

const Actor = (props) => {
	return(
		<div className="cell Actor">
			<Link key={props.id} to={'/person/' + props.id}>
				<div className="imageContainer">
					 <img src={
					 	props.image ? 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/' + props.image : 'https://imgplaceholder.com/600x900/cccccc/757575/fa-user'
					 } alt={props.name}/>
				</div>
				<div className="actorContent">
					<h6>{props.name}</h6>
					<span>{props.character}</span>
				</div>
			</Link>
		</div>
	)
}

export default Actor;