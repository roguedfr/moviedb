import React, {Component} from 'react';
import Axios from 'axios';

import './Similar.scss';
import Film from '../Film/Film';

class SimliarTitles extends Component {
	constructor(props){
		super(props);
		this.state = {
			id : this.props.MovieId,
			similar : [],
		}
	}

	componentWillMount(){
		Promise.all([
			Axios.get('movie/' + this.state.id + '/similar?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
		])
			.then(([similarResponse]) => {
				this.setState({similar: similarResponse.data.results});
			});
	}

	componentWillReceiveProps(nextProps){
		if (nextProps.MovieId !== this.state.id) {
			this.setState({id: nextProps.MovieId})
		}
		
	}
	
	componentDidUpdate(prevProps, prevState){
		if (prevState.id !== this.state.id) {
		    Promise.all([
				Axios.get('movie/' + this.state.id + '/similar?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
			])
				.then(([similarResponse]) => {
				this.setState({similar: similarResponse.data.results});
			});
		  }
	}

	render(){
		return(
			<React.Fragment>
				<div className="SimilarList section text-center">
					<div className="small-12 cell grid-x align-center">
						<h3 className="Title">Similar Titles</h3>
					</div>
					<div className="small-12 cell grid-x large-up-4">
						{this.state.similar
							.sort((a, b) => b.popularity - a.popularity)
							.slice(0, 8)
							.map((filmKey, i) => {
								return(
									<Film
										key={filmKey.id}
										id={filmKey.id}
										Title={filmKey.title}
										BackgroundImage={filmKey.backdrop_path}/>
								)
							})}
					</div>
				</div>
			</React.Fragment>
		)
	}
}

export default SimliarTitles;