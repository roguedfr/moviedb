import React, { Component } from 'react';
import Axios from 'axios';

import Film from './Film/Film';

class Films extends Component {

	state = {
		films: []
	}

	componentWillMount(){
		Promise.all([
			Axios.get('/person/' + this.props.Id + '/combined_credits?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
		])
			.then(([filmResponse]) => {
				this.setState({films: filmResponse.data.cast});
			});
	}

	render (){
		return (
			<div className="TopFilms section">
				<div className="grid-container-full">
					<div className="grid-x grid-margin-x">
						<div className="cell small-12 text-center">
							<h2>Top Credits</h2>
						</div>
						<div className="small-12 cell grid-x large-up-4">
							{this.state.films.filter(filmKey => (filmKey.media_type === 'movie' || filmKey.episode_count >= 6))
											.sort((a, b) => b.popularity - a.popularity)
											.slice(0, 8)
											.map((filmKey, i) => {
								return <Film
											key={filmKey.id}
											id={filmKey.id}
											Title={filmKey.title}
											TVTitle={filmKey.name}
											BackgroundImage={filmKey.backdrop_path}/>
							})}
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default Films;