import React from 'react';
import { Link } from 'react-router-dom';

import './Film.scss';

const Film = (props) => {
	return (
		<div className="cell Film" style={{backgroundImage:'url(https://image.tmdb.org/t/p/original/'+props.BackgroundImage+')'}}>
			<Link key={props.id} to={'/movie/' + props.id}>
				<span>{props.Title ? props.Title : props.TVTitle}</span>
			</Link>
		</div>
	)
}

export default Film;