import React, {Component} from 'react';
import Axios from 'axios';
import Aux from '../../hoc/Auxilary';

import './Content.scss';

class Content extends Component {
	state = {
		id: this.props.Id,
		credits: [],
		social: [],
		cast: [],
		crew: [],
	}

	componentWillMount(){
		if (this.props.PageType === "/movie/:movieId"){
		Promise.all([
			Axios.get('/movie/' + this.state.id + '/credits?api_key=a5ccc05c247dfd2c49d147b2a043eafa'),
			Axios.get('/movie/' + this.state.id + '/external_ids?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
		])
			.then(([creditsResponse, socialReponse]) => {
				this.setState({credits: creditsResponse.data.crew, social: socialReponse.data});
			});
		} else if (this.props.PageType === "/person/:personId"){
			Promise.all([
			Axios.get('/person/' + this.state.id + '/combined_credits?api_key=a5ccc05c247dfd2c49d147b2a043eafa'),
			Axios.get('/person/' + this.state.id + '/external_ids?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
		])
			.then(([combinedCreditsResponse, socialReponse, ]) => {
				this.setState({cast: combinedCreditsResponse.data.cast, crew: combinedCreditsResponse.data.crew , social: socialReponse.data});
			});
		}
	}


	componentWillReceiveProps(nextProps){
		if (nextProps.Id !== this.state.id) {
			this.setState({id: nextProps.Id})
		}
		
	}

	componentDidUpdate(prevProps, prevState){
		if (prevState.id !== this.state.id) {
		    Promise.all([
				Axios.get('movie/' + this.state.id + '/credits?api_key=a5ccc05c247dfd2c49d147b2a043eafa'),
				Axios.get('movie/' + this.state.id + '/external_ids?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
			])
				.then(([creditsResponse, socialReponse]) => {
					this.setState({credits: creditsResponse.data.crew, social: socialReponse.data});
				});
		}
	}

	render(){

	const formatter = new Intl.NumberFormat('en-US', {
	  style: 'currency',
	  currency: 'USD',
	  minimumFractionDigits: 0
	});

	const budget = formatter.format(this.props.Budget);
	const revenue = formatter.format(this.props.Revenue);

	const time_convert = (num) =>
	 { 
	  var hours = Math.floor(num / 60);  
	  var minutes = num % 60;
	  return hours + "hr " + minutes + "mins";         
	}

	let Director = this.state.credits.filter(crewKey => crewKey.job === "Director");

    let Screenplay = this.state.credits.filter(crewKey => crewKey.job === "Screenplay");

    let Gender = this.props.Gender

   	let descriptionTitle = null
   	let FirstDetail = null
    let SecondDetail = null
    let ThirdDetail = null
    let FourthDetail = null
    let FifthDetail = null
    let BackGround = null

    if (Gender === 1){
		Gender = 'Female'
	} else if (Gender === 2){
		Gender = 'Male'
	}

    // descriptionTitle

   	if (this.props.PageType === "/movie/:movieId"){
    	descriptionTitle = <h5>Synopsis</h5>;
    	FirstDetail = 	<Aux>
	    					<div className="cell small-6">
								<strong>Director</strong>
							</div>
							<div className="cell small-6">
								{Director.map(function (crewKey, i) {
					            console.log(crewKey, i, Director.length)
					            if (Director.length === i+1){
					            	return <span key={crewKey.id}>{crewKey.name}</span>
					            } else {
					            	return <span key={crewKey.id}>{crewKey.name}<br/></span>
					            }
					          	})}
							</div>
						</Aux>
		SecondDetail = 	<Aux>
							<div className="cell small-6">
								<strong>Writer</strong>
							</div>
							<div className="cell small-6">
								{Screenplay.map(function (crewKey, i) {
					            	if (Screenplay.length === i+1){
						            	return <span key={crewKey.id}>{crewKey.name}</span>
						            } else {
						            	return <span key={crewKey.id}>{crewKey.name}<br/></span>
						            }
					          	})}
							</div>
						</Aux>
		ThirdDetail = 	<Aux>
							<div className="cell small-6">
								<strong>Runtime</strong>
							</div>
							<div className="cell small-6">
								{time_convert(this.props.Runtime)}
							</div>
						</Aux>
		FourthDetail =	<Aux>
							<div className="cell small-6">
								<strong>Budget</strong>
							</div>
							<div className="cell small-6">
								{budget}
							</div>
						</Aux>
		FifthDetail = 	<Aux>
							<div className="cell small-6">
								<strong>Revenue</strong>
							</div>
							<div className="cell small-6">
								{revenue}
							</div>
						</Aux>
		BackGround = <div className="ContentBackground" style={{backgroundImage:'url(https://image.tmdb.org/t/p/original'+this.props.BackgroundImage+')'}}></div>
    } else if (this.props.PageType === "/person/:personId"){
    	descriptionTitle = <h5>Biography - ({this.props.Department})</h5>
    	FirstDetail = 	<Aux>
	    					<div className="cell small-6">
								<strong>Date of Birth</strong>
							</div>
							<div className="cell small-6">
								{this.props.Birthday}
							</div>
						</Aux>
		SecondDetail = 	<Aux>
							<div className="cell small-6">
								<strong>Gender</strong>
							</div>
							<div className="cell small-6">
								{Gender}
							</div>
						</Aux>
		ThirdDetail = 	<Aux>
							<div className="cell small-6">
								<strong>Place of Birth</strong>
							</div>
							<div className="cell small-6">
								{this.props.BirthPlace}
							</div>
						</Aux>
		FourthDetail = <Aux>
							<div className="cell small-6">
								<strong>Known Credits</strong>
							</div>
							<div className="cell small-6">
								{Object.keys(this.state.cast).length + Object.keys(this.state.crew).length}
							</div>
						</Aux>
		FifthDetail = 	<Aux>
							<div className="cell small-6">
								<strong>Official Site</strong>
							</div>
							<div className="cell small-6">
								{this.props.Homepage ? this.props.Homepage : 'n/a'}
							</div>
						</Aux>
		BackGround = <div className="Person"></div>
    };

	return (
		
	<div className="Content section">
		{BackGround}
		<div className="grid-container">
			<div className="small-12 cell grid-x MovieContent ">
				<div className="cell small-4">
					<img src={'https://image.tmdb.org/t/p/original' + this.props.PosterImage} alt={this.props.Name + 'Poster'}/>
				</div>
				<div className="cell small-8 MovieInfo grid-x grid-padding-x align-middle">
					<div className="cell small-9">
						<h2>{this.props.Name}</h2>
					</div>
					<div className="cell small-3 Social">
						{this.state.social.facebook_id ? <a href={'https://www.facebook.com/' + this.state.social.facebook_id} target="_blank" rel="noopener noreferrer"><i class="fab fa-facebook-f"></i></a> : null}
						{this.state.social.twitter_id ? <a href={'https://www.twitter.com/' + this.state.social.twitter_id} target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a> : null}
						{this.state.social.instagram_id ? <a href={'https://www.instagram.com/' + this.state.social.instagram_id} target="_blank" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a> : null}
						{this.props.Link ? <a href={this.props.Link} target="_blank" rel="noopener noreferrer"><i class="fas fa-link"></i></a> : null}						
					</div>
					<div className="cell small-12">
						{descriptionTitle}
						<p>{this.props.Description}</p>
					</div>
					<div className="cell small-7 grid-x grid-margin-x MovieInfoBullets">
						<div className="cell small-12 grid-x align-middle">
							{FirstDetail}
						</div>
						<div className="cell small-12 grid-x align-middle">
							{SecondDetail}
						</div>
						<div className="cell small-12 grid-x align-middle">
							{ThirdDetail}
						</div>
						<div className="cell small-12 grid-x align-middle">
							{FourthDetail}
						</div>
						<div className="cell small-12 grid-x align-middle">
							{FifthDetail}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	)}
}

export default Content;