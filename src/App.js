import React, {Component} from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';

import './App.scss';

import Aux from './hoc/Auxilary';
import ScrollToTop from './hoc/Scroll';

import Movie from './container/Movie/Movie';
import Person from './container/Person/Person';
import Auth from './container/Auth/Auth';

class App extends Component {
  render (){
    return (
    	<BrowserRouter>
    		<ScrollToTop />
	      	<Aux>
	      		<Switch>
	      			<Route path="/movie/:movieId" component={Movie}/>
	      			<Route path="/person/:personId" component={Person}/>
	      			<Route path="/auth" component={Auth}/>
	      		</Switch>
	      	</Aux>
    	</BrowserRouter>
    )
  }
}

export default App;
