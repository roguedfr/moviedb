import React, {Component} from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index'

import Input from '../../UI/Input/Input';

import './Auth.scss'

class Auth extends Component {
	state = {
		loginForm:{
			username: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Login Username',
				},
				value: '',
				validation:{
					required: true,
					minLength: 6
				},
				valid: false,
				touched: false,
				errorMessage: 'Please enter a valid username'
			},
			password: {
				elementType: 'input',
				elementConfig: {
					type: 'password',
					placeholder: 'Password',
				},
				value: '',
				validation:{
					required: true,
					minLength: 6
				},
				valid: false,
				touched: false,
				errorMessage: 'Password must be a minimum of 6 characters'
			},
		},
		formIsValid:false,
		formSuccess: false,
		formFail:false
	}

	checkValidation(value, rules){
		let isValid = true

		if (rules.required){
			isValid = value.trim() !== '' && isValid;
		}

		if (rules.minLength){
			isValid = value.length >= rules.minLength && isValid;
		}

		if (rules.maxLength){
			isValid = value.length <= rules.minLength && isValid;
		}

		if (rules.email){
			isValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
		}

		return isValid;
	}

	inputChangedHandler = (event, inputIdentifier) => {
		const updatedLoginForm = {
			...this.state.loginForm,
			[inputIdentifier]: {
				...this.state.loginForm[inputIdentifier],
				value: event.target.value,
				valid: this.checkValidation(event.target.value, this.state.loginForm[inputIdentifier].validation),
				touched: true
			}
		};
		this.setState({loginForm: updatedLoginForm});
	}

	submitHandler = (event) => {
		event.preventDefault();
		this.props.onAuth(this.state.loginForm.username.value, this.state.loginForm.password.value);
	}

	render(){
		const formElementsArray = [];
		for (let key in this.state.loginForm){
			formElementsArray.push({
				id: key,
				config: this.state.loginForm[key],
			})
		}

		return(
			<div className="Auth">
				<form onSubmit={this.submitHandler}>
					{formElementsArray.map(formElement => (
					<Input 
							key={formElement.id}
							elementType={formElement.config.elementType}
							elementConfig={formElement.config.elementConfig}
							value={formElement.config.value}
							invalid={!formElement.config.valid}
							shouldValidate={formElement.config.validation}
							touched={formElement.config.touched}
							changed={(event) => this.inputChangedHandler(event, formElement.id)}
							errorMessage={formElement.config.errorMessage}/>
					))}
					<div className="text-right">
						<button className="hollow button primary">Login</button>
					</div>
				</form>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onAuth: (username, password) => dispatch(actions.auth(username, password))
	};
};

export default connect(null, mapDispatchToProps)(Auth);