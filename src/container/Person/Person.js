import React, { Component } from 'react';
import Axios from 'axios';

import Aux from '../../hoc/Auxilary';
import Content from '../../components/Content/Content';
import Films from '../../components/Films/Films';

class Person extends Component {
	
	state = {
		id: this.props.match.params.personId,
		person: []
	}

	componentWillMount(){
		Promise.all([
			Axios.get('person/' + this.state.id + '?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
		])
			.then(([personResponse]) => {
				this.setState({person: personResponse.data});
			});
	}

	render(){
		console.log(this.props.match);
		return(
			<Aux>
				<Content
					Id = {this.state.id} 
					Name={this.state.person.name}
					PosterImage={this.state.person.profile_path}
					BackgroundImage={this.state.person.backdrop_path}
					Description={this.state.person.biography}
					Budget={this.state.person.budget}
					Revenue={this.state.person.revenue}
					Runtime={this.state.person.runtime}
					ReleaseDate={this.state.release}
					Link={this.state.person.homepage}
					PageType={this.props.match.path}
					Department={this.state.person.known_for_department}
					Birthday={this.state.person.birthday}
					BirthPlace={this.state.person.place_of_birth}
					Gender={this.state.person.gender}
					Homepage={this.state.person.homepage}/>
				<Films
					Id = {this.state.id}/>
			</Aux>
		)
	}
}

export default Person;