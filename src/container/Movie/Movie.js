import React, {Component} from 'react';
import Axios from 'axios';

import Hero from '../../components/Hero/Hero';
import Content from '../../components/Content/Content';
import Actors from '../../components/ActorList/ActorList';
import SimilarTitles from '../../components/Films/Similar/Similar';

class Movie extends Component {

	state = {
		id: this.props.match.params.movieId,
		movie:[],
		year: '2019',
		listAdded: false,
		bookmark: false,
		watchlistAdded: false,
		rating: 0,
		release: 'June 7, 2019',
		showSettings: false
	};

	componentWillMount(){
		Promise.all([
				Axios.get('movie/' + this.state.id + '?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
			])
				.then(([movieResponse, videoResponse]) => {
					this.setState({movie: movieResponse.data});
				});
	}

	componentDidUpdate(prevProps, prevState){
		if (prevState.id !== this.props.match.params.movieId) {
		    Promise.all([
				Axios.get('movie/' + this.state.id + '?api_key=a5ccc05c247dfd2c49d147b2a043eafa')
			])
				.then(([movieResponse, videoResponse]) => {
					this.setState({id: this.props.match.params.movieId, movie: movieResponse.data});
				});
		  }
	}

	listAddedHandler = () => {
		this.setState(prevState => ({listAdded: !prevState.listAdded}));
	}

	bookmarkAddedHandler = () => {
		this.setState(prevState => ({bookmark: !prevState.bookmark}));
	}

	watchListAddedHandler = () => {
		this.setState(prevState => ({watchlistAdded: !prevState.watchlistAdded}));
	}

	showSettingsAddedHandler = () => {
		this.setState(prevState => ({showSettings: !prevState.showSettings}));
	}

	ratingHandler= (value) =>{
		this.setState({rating: value + 1})
	}

	movieIdHandler = (movie) => {
		this.setState({id: movie})
	}


	render () {
		return (
			<React.Fragment>
			<Hero 
				BackgroundImage={this.state.movie.backdrop_path}
				Name={this.state.movie.title}
				Year={this.state.year}
				MovieId = {this.state.id}
				UserRating = {this.state.movie.vote_average}
				ListAdded = {this.state.listAdded}
				ListClicked = {this.listAddedHandler}
				BookmarkAdded = {this.state.bookmark}
				BookmarkClicked = {this.bookmarkAddedHandler}
				WatchListAdded = {this.state.watchlistAdded}
				WatchListClicked = {this.watchListAddedHandler}
				Rating = {this.state.rating}
				RatingChoice = {this.ratingHandler}
				/>
			<Content
				Id = {this.state.id} 
				Name={this.state.movie.title}
				PosterImage={this.state.movie.poster_path}
				BackgroundImage={this.state.movie.backdrop_path}
				Description={this.state.movie.overview}
				Budget={this.state.movie.budget}
				Revenue={this.state.movie.revenue}
				Runtime={this.state.movie.runtime}
				ReleaseDate={this.state.release}
				Link={this.props.homepage}
				PageType={this.props.match.path}/>
			<Actors MovieId = {this.state.id}/>
			<SimilarTitles MovieId = {this.state.id}/>
			</React.Fragment>
		)
	};
}

export default Movie;